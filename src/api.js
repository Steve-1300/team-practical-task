const Token = '97wjqj4z'

export async function getPoints() {
    let res = await fetch('https://dt.miet.ru/ppo_it_final', {
        // mode: 'no-cors',
        headers: {
            method: 'GET',
            "Content-Type": "application/json",
            'X-Auth-Token': Token
        }
    })
    console.log(res);
    let data = await res.json()
    return data
}