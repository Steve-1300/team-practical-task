import requests 
import math 
token = input()
url = 'https://dt.miet.ru/ppo_it_final' 
headers = {'X-Auth-Token': token} 
 
req = requests.get(url, headers=headers) 
a = req.json() 
 
w = [] 
for i in range(len(a['message'])): 
    t = a['message'][i] 
    for q in range(len(t['points'])): 
        w.append(t['points'][q]['SH']) 
 
w.append(192) 
Len = [] 
time = [] 
use_res = []
for i in range(len(a['message'])): 
    t = a['message'][i] 
    for q in range(len(t['points'])): 
        r = t['points'][q]['distance'] 
        Len.append(r) 
        e = math.ceil(r / (2 * (200 / sum(w)))) 
        time.append(e) 
        w = w[1:] 
        use_res.append(e * 80) 
count_res = sum(time) * 80
summa = 0
qwe = []
for i in range(len(use_res)):
    summa += use_res[i]
    qwe.append(count_res - summa)

print(sum(time), "days")
print(", ".join(list(map(str, qwe[:-1]))), "resources")